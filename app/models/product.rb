class Product < ApplicationRecord
	belongs_to :user,optional: true
    has_many_attached :images	
	enum category: [:electronics, :footwear, :jewellery, :books, :fashion_acessories, :sports_good]
	ratyrate_rateable "best" , "low", "average"
	has_many :ratings,  foreign_key: 'rateable_id', class_name: 'Rate' 

	def overall_ratings(product)
		
        array = Rate.where(rateable_id: id, rateable_type: 'Product')
		stars = array.map {|product| product.stars }
		star_count = stars.count
		stars_total = stars.inject(0){|sum,x| sum + x }
		score = stars_total / (star_count.nonzero? || 1)
	end

	

    def self.search(search =nil,search1=nil)		
		if search.present? && search1.present?
			where("name like ? and category like ?", "%#{search}%", "%#{search1}%")
		elsif search.present?		
			where("name like ? ", "%#{search}%")	
		elsif search1.present?

			where("category like ? ", "%#{search1}%")
		else
			products = Product.all
		end
	end
end
