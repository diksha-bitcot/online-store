class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  enum role: [:superadmin, :admin, :user]
  has_many :products, dependent: :destroy
  belongs_to :admin, :optional => true, class_name: 'User'
  #has_many :ratings, foreign_key: "rater_id" ,class_name: "Rate"
   has_one :page
  ratyrate_rater
end


