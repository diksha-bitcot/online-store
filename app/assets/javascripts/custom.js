// _category page
$(document).ready(function(){
		$('.edit-input').keypress(function(event){
        
			if(event.keyCode == '13') {
				$(this.previousElementSibling).text(this.value)
				$(this).toggle();
		        $(this).closest('span').toggle()
		        $('.text-field').show()

				event.preventDefault();
				var id = $(this).attr('data-id')
				
				$.ajax({
					url: '/products/'+id, 
					type: "patch", 
					data: {name: $(this).val(), product_id: $(this).attr("product.id")},
					dataType: "JSON",
					success: function(msg) {
						
					}

				});
			}
		});
	});

// Index page
function  myFunction() {
		
		
		var search= {
			'search': $('#search').val(),
			'search1': $('#search1').val(),
		};;

		
		
		$.ajax({
			url: "/products",
			type: "get",
			dataType: "json",
			data: search,
			success: function(data) {
				$('.data').html(data.html)
			},
			error: function(data) {}
		})
		return false
	}
	$('.edit-input').hide()

	$('td').on("click", function() {
		$(this).find('span').toggle();
		$(this).find('input').toggle()
		$(this).find('input').show()

		
	});