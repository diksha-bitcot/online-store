class ProductsController < ApplicationController
    skip_before_action :verify_authenticity_token  
	before_action :authenticate_user! , except: [:index]

	def index
		@products = Product.search(params[:search], params[:search1])		
		html =render_to_string :partial => 'products/category', locals: {:search => @products}
		respond_to do |format|
			format.html
			format.json { render json: {html: html} }
		end
	end

	def new
		@product = Product.new
	end

	def create
		@product = Product.new(product_params)
        if @product.save
			redirect_to @product
		else
			render 'new'
		end
	end

	def show
		@product = Product.find (params[:id])

    end

	def edit
		@product = Product.find (params[:id])
		
    end

    def update
    	@product = Product.find(params[:id])
    	
        if @product.update(product_params)
    		redirect_to @product
    	else
    		render 'edit'
    	end
	end

	def destroy
		@product = Product.find(params[:id])
		@product.destroy
		redirect_to products_path
	end



	private
	def product_params
    	params.require(:product).permit(:name, :category, images:[])
    end
end


