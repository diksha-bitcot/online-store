class AdminController < ApplicationController
	def index
		@user = User.admin
		render 'superadmin/users/index'
	end
end
