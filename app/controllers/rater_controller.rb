class RaterController < ApplicationController
	skip_before_action :verify_authenticity_token

  def create

    if current_user
    
      obj = params[:klass].classify.constantize.find(params[:id])
      obj.rate params[:score].to_f, current_user, params[:dimension]
       @product = obj
     
    end

    respond_to do |format|
      # format.html
      format.js 
    end
  end
end
