module Superadmin
  class SuperadminApplicationController < ApplicationController
  	
  	protect_from_forgery with: :exception

    before_action :authenticate_user!
    before_action :authenticate_superadmin

    def authenticate_superadmin
      if current_user.superadmin?
        true
      else
      	redirect_to root_path
        
      end
    end
   end
end
