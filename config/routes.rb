Rails.application.routes.draw do
  post '/rate' => 'rater#create', :as => 'rate'
  get 'home/index'
  root to: "home#index"
  resources :products
  resources :categories

  devise_for :users

  namespace :superadmin do
  	resources :users do
  		collection do
  			get :admins
  		end
  	end
  end
end
